use entities::todo::Todo;
use std::io;
use uuid::Uuid;
use use_cases::data_store_trait::DataStoreTrait;
use use_cases::create_new_todo::CreateNewTodo;

pub struct RetrieveTodo {}

impl RetrieveTodo {

    pub fn retrieve<T: DataStoreTrait>(todo_db: &mut T, id: Uuid) -> Result<Todo, io::Error> {
        Ok(todo_db.load(id))
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use interactors::in_memory_kvdb::InMemoryKVDB;

    #[test]
    fn retrieve_returns_correct_todo() {
        let mut db = InMemoryKVDB::new();
        let todo = CreateNewTodo::create_from(&mut db, "Hello world".to_string()).unwrap();
        let xodo = RetrieveTodo::retrieve(&mut db, todo.id).unwrap();
        assert_eq!(todo.description, xodo.description);
        assert_eq!(todo.id, xodo.id);
        assert_eq!(todo.complete, xodo.complete);
        assert_eq!(todo.visible, xodo.visible);
    }

    #[test]
    fn retrieve_returns_todo_object_works_with_many_fetches() {
        let mut db = InMemoryKVDB::new();

        for i in 0..10 {
            let desc = format!("Hello {}", i);
            let t = CreateNewTodo::create_from(&mut db, desc.to_string()).unwrap();
            assert_eq!(t.description, desc);
        }
    }
}
