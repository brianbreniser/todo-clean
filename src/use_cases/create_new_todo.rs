use entities::todo::Todo;
use std::io;
use use_cases::data_store_trait::DataStoreTrait;

pub struct CreateNewTodo{}

impl CreateNewTodo {
    pub fn create_from<T: DataStoreTrait>(todo_db: &mut T, s: String) -> Result<Todo, io::Error> {
        let my_todo = Todo::from(s);
        todo_db.save(my_todo.clone());
        Ok(my_todo)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use interactors::in_memory_kvdb::InMemoryKVDB;

    #[test]
    fn has_create_from_function() {
        let mut db = InMemoryKVDB::new();
        CreateNewTodo::create_from(&mut db, "Hello world".to_string()).unwrap();
    }

    #[test]
    fn create_from_returns_todo_object() {
        let mut db = InMemoryKVDB::new();
        let t = CreateNewTodo::create_from(&mut db, "Hello world".to_string()).unwrap();
        assert_eq!(t.description, "Hello world");
    }

    #[test]
    fn create_from_returns_todo_object_works_multiple_times() {
        let mut db = InMemoryKVDB::new();
        let t = CreateNewTodo::create_from(&mut db, "Hello world".to_string()).unwrap();
        assert_eq!(t.description, "Hello world");

        let t = CreateNewTodo::create_from(&mut db, "Hello world1".to_string()).unwrap();
        assert_eq!(t.description, "Hello world1");

        let t = CreateNewTodo::create_from(&mut db, "Hello world2".to_string()).unwrap();
        assert_eq!(t.description, "Hello world2");
    }
}
