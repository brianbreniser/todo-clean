use entities::todo::Todo;
use std::io;
use uuid::Uuid;
use use_cases::data_store_trait::DataStoreTrait;

pub struct TodoStore {
    // We bring in the db type at runtime
    // and use dynamic dispatch to make db changes
    todo_db: Box<DataStoreTrait>,
}

impl TodoStore {
    pub fn new(db: Box<DataStoreTrait>) -> TodoStore {
        TodoStore { todo_db: db }
    }

    pub fn create_from(&mut self, s: String) -> Result<Todo, io::Error> {
        let my_todo = Todo::from(s);
        self.todo_db.save(my_todo.clone());
        Ok(my_todo)
    }

    pub fn get(&mut self, id: Uuid) -> Todo {
        self.todo_db.load(id)
    }

    pub fn update_description(&mut self, id: Uuid, s: String) -> Result<Todo, io::Error> {
        let t = self.todo_db.load(id);
        let new_t = t.with_description(s);
        self.todo_db.save(new_t.clone());
        Ok(new_t)
    }

    pub fn mark_complete(&mut self, id: Uuid) -> Result<Todo, io::Error> {
        let t = self.todo_db.load(id);
        let new_t = t.mark_complete();
        self.todo_db.save(new_t.clone());
        Ok(new_t)
    }

    pub fn mark_incomplete(&mut self, id: Uuid) -> Result<Todo, io::Error> {
        let t = self.todo_db.load(id);
        let new_t = t.mark_incomplete();
        self.todo_db.save(new_t.clone());
        Ok(new_t)
    }

    pub fn mark_invisible(&mut self, id: Uuid) -> Result<Todo, io::Error> {
        let t = self.todo_db.load(id);
        let new_t = t.mark_invisible();
        self.todo_db.save(new_t.clone());
        Ok(new_t)
    }

    pub fn mark_visible(&mut self, id: Uuid) -> Result<Todo, io::Error> {
        let t = self.todo_db.load(id);
        let new_t = t.mark_visible();
        self.todo_db.save(new_t.clone());
        Ok(new_t)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use interactors::in_memory_kvdb::InMemoryKVDB;

    #[test]
    fn has_create_from_function() {
        let db = InMemoryKVDB::new();
        let mut todo_list = TodoStore::new(Box::new(db));
        todo_list
            .create_from("This is a new todo item".to_string())
            .expect("create_froming a new item failed epically!");
    }

    #[test]
    fn create_from_returns_uuid() {
        let db = InMemoryKVDB::new();
        let mut todo_list = TodoStore::new(Box::new(db));
        let _x: Todo = todo_list
            .create_from("This is a new todo item".to_string())
            .expect("create_froming a new item failed epically!");
    }

    #[test]
    fn can_get_by_uuid() {
        let db = InMemoryKVDB::new();
        let mut todo_list = TodoStore::new(Box::new(db));
        let td: Todo = todo_list
            .create_from("This is a new todo item".to_string())
            .expect("create_froming a new item failed epically!");

        let ret_todo = todo_list.get(td.id);

        assert_eq!(td.id, ret_todo.id);
    }

    #[test]
    fn can_get_by_uuid_and_description_is_the_same() {
        let db = InMemoryKVDB::new();
        let mut todo_list = TodoStore::new(Box::new(db));
        let td: Todo = todo_list
            .create_from("This is a new todo item".to_string())
            .expect("create_froming a new item failed epically!");

        let ret_todo = todo_list.get(td.id);

        assert_eq!(td.id, ret_todo.id);
        assert_eq!(ret_todo.description, "This is a new todo item");
    }

    #[test]
    fn can_update_the_description() {
        let db = InMemoryKVDB::new();
        let mut todo_list = TodoStore::new(Box::new(db));
        let td: Todo = todo_list
            .create_from("This is a new todo item".to_string())
            .expect("create_froming a new item failed epically!");

        let ret_todo = todo_list.get(td.id);

        assert_eq!(td.id, ret_todo.id);
        assert_eq!(ret_todo.description, "This is a new todo item");

        todo_list
            .update_description(td.id, "New desc".to_string())
            .unwrap();

        let new_todo = todo_list.get(td.id);

        assert_eq!(td.id, new_todo.id);
        assert_eq!(new_todo.description, "New desc");
    }

    #[test]
    fn can_update_check_mark_to_complete() {
        let db = InMemoryKVDB::new();
        let mut todo_list = TodoStore::new(Box::new(db));
        let td: Todo = todo_list
            .create_from("This is a new todo item".to_string())
            .expect("create_froming a new item failed epically!");

        let ret_todo = todo_list.get(td.id);

        assert_eq!(ret_todo.complete, false);

        todo_list.mark_complete(td.id).unwrap();
        let new_todo = todo_list.get(td.id);

        assert_eq!(new_todo.complete, true);
    }

    #[test]
    fn can_update_check_mark_to_incomplete() {
        let db = InMemoryKVDB::new();
        let mut todo_list = TodoStore::new(Box::new(db));
        let td: Todo = todo_list
            .create_from("This is a new todo item".to_string())
            .expect("create_froming a new item failed epically!");

        let ret_todo = todo_list.get(td.id);

        assert_eq!(ret_todo.complete, false);

        todo_list.mark_incomplete(td.id).unwrap();
        let new_todo = todo_list.get(td.id);

        assert_eq!(new_todo.complete, false);
    }

    #[test]
    fn check_mark_toggles_well() {
        let db = InMemoryKVDB::new();
        let mut todo_list = TodoStore::new(Box::new(db));
        let td: Todo = todo_list
            .create_from("This is a new todo item".to_string())
            .expect("create_froming a new item failed epically!");

        let ret_todo = todo_list.get(td.id);
        assert_eq!(ret_todo.complete, false);

        todo_list.mark_incomplete(td.id).unwrap();

        let ret_todo = todo_list.get(td.id);
        assert_eq!(ret_todo.complete, false);

        todo_list.mark_complete(td.id).unwrap();

        let ret_todo = todo_list.get(td.id);
        assert_eq!(ret_todo.complete, true);

        todo_list.mark_incomplete(td.id).unwrap();

        let ret_todo = todo_list.get(td.id);
        assert_eq!(ret_todo.complete, false);
    }

    #[test]
    fn todo_item_starts_visible() {
        let db = InMemoryKVDB::new();
        let mut todo_list = TodoStore::new(Box::new(db));
        let td: Todo = todo_list
            .create_from("This is a new todo item".to_string())
            .expect("create_froming a new item failed epically!");

        let ret_todo = todo_list.get(td.id);

        assert_eq!(ret_todo.is_visible(), true);
    }

    #[test]
    fn can_update_to_invisible() {
        let db = InMemoryKVDB::new();
        let mut todo_list = TodoStore::new(Box::new(db));
        let td: Todo = todo_list
            .create_from("This is a new todo item".to_string())
            .expect("create_froming a new item failed epically!");

        let ret_todo = todo_list.get(td.id);
        assert_eq!(ret_todo.complete, false);

        todo_list.mark_invisible(td.id).unwrap();

        let new_todo = todo_list.get(td.id);
        assert_eq!(new_todo.is_visible(), false);
    }

    #[test]
    fn can_update_to_visible() {
        let db = InMemoryKVDB::new();
        let mut todo_list = TodoStore::new(Box::new(db));
        let td: Todo = todo_list
            .create_from("This is a new todo item".to_string())
            .expect("create_froming a new item failed epically!");

        let ret_todo = todo_list.get(td.id);
        assert_eq!(ret_todo.complete, false);

        todo_list.mark_visible(td.id).unwrap();

        let new_todo = todo_list.get(td.id);
        assert_eq!(new_todo.is_visible(), true);
    }

    #[test]
    fn invisibility_toggles_well() {
        let db = InMemoryKVDB::new();
        let mut todo_list = TodoStore::new(Box::new(db));
        let td: Todo = todo_list
            .create_from("This is a new todo item".to_string())
            .expect("create_froming a new item failed epically!");

        let ret_todo = todo_list.get(td.id);
        assert_eq!(ret_todo.is_visible(), true);

        todo_list.mark_invisible(td.id).unwrap();

        let ret_todo = todo_list.get(td.id);
        assert_eq!(ret_todo.is_visible(), false);

        todo_list.mark_visible(td.id).unwrap();

        let ret_todo = todo_list.get(td.id);
        assert_eq!(ret_todo.is_visible(), true);
    }
}
