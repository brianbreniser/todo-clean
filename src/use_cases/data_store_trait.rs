use entities::todo::Todo;
use uuid::Uuid;

// For external structures to implement
// We use this for dependency inversion, we don't want to rely on the db type
pub trait DataStoreTrait {
    fn save(&mut self, my_todo: Todo);
    fn load(&mut self, id: Uuid) -> Todo;
}

