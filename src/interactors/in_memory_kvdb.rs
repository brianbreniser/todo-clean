use entities::todo::Todo;
use std::collections::HashMap;
// use use_cases::todo_store::DataStoreTrait;
use use_cases::data_store_trait::DataStoreTrait;
use uuid::Uuid;

pub struct InMemoryKVDB {
    dbstore: HashMap<Uuid, Todo>,
}

impl InMemoryKVDB {
    pub fn new() -> InMemoryKVDB {
        InMemoryKVDB {
            dbstore: HashMap::new(),
        }
    }
}

impl DataStoreTrait for InMemoryKVDB {
    fn save(&mut self, my_todo: Todo) {
        self.dbstore.insert(my_todo.id, my_todo);
    }

    fn load(&mut self, id: Uuid) -> Todo {
        self.dbstore.get(&id).unwrap().clone()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn new_works() {
        let _db = InMemoryKVDB::new();
    }

    #[test]
    fn insert_will_insert_without_complaint() {
        let mut db = InMemoryKVDB::new();
        let t = Todo::from("hello there".to_string());
        db.save(t);
    }

    #[test]
    fn find_will_get_what_we_inserted() {
        let mut db = InMemoryKVDB::new();
        let t = Todo::from("hello there".to_string());
        let id = t.id.clone();
        db.save(t);
        let x = db.load(id);

        assert_eq!(id, x.id);
    }
}
