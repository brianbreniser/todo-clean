use uuid::Uuid;

#[derive(Clone, Debug)]
pub struct Todo {
    pub id: Uuid,
    pub description: String,
    pub complete: bool,
    pub visible: bool,
}

impl Todo {
    pub fn from(s: String) -> Todo {
        Todo {
            id: Uuid::new_v4(),
            description: s,
            complete: false,
            visible: true,
        }
    }

    pub fn is_complete(&self) -> bool {
        self.complete
    }

    pub fn is_visible(&self) -> bool {
        self.visible
    }

    pub fn mark_complete(&self) -> Todo {
        Todo {
            id: self.id,
            description: self.description.clone(),
            complete: true,
            visible: self.visible,
        }
    }

    pub fn mark_incomplete(&self) -> Todo {
        Todo {
            id: self.id,
            description: self.description.clone(),
            complete: false,
            visible: self.visible,
        }
    }

    pub fn mark_visible(&self) -> Todo {
        Todo {
            id: self.id,
            description: self.description.clone(),
            complete: self.complete,
            visible: true,
        }
    }

    pub fn mark_invisible(&self) -> Todo {
        Todo {
            id: self.id,
            description: self.description.clone(),
            complete: self.complete,
            visible: false,
        }
    }

    pub fn with_description(&self, s: String) -> Todo {
        Todo {
            id: self.id,
            description: s,
            complete: self.complete,
            visible: self.visible,
        }
    }
}

#[cfg(test)]
mod todo_tests {
    use super::*;

    #[test]
    fn new_todo_works() {
        let my_todo = Todo::from("hello, todo world!".to_string());

        assert_eq!(my_todo.description, "hello, todo world!");
    }

    #[test]
    fn new_todo_works_empty_string() {
        let my_todo = Todo::from("".to_string());

        assert_eq!(my_todo.description, "");
    }

    #[test]
    fn mark_complete_function_makes_todo_complete() {
        let my_todo = Todo::from("hello, todo world!".to_string());

        assert!(!my_todo.is_complete());
        assert!(my_todo.mark_complete().is_complete());
        assert!(my_todo.mark_complete().is_complete());
    }

    #[test]
    fn mark_incomplete_function_makes_todo_incomplete() {
        let my_todo = Todo::from("hello, todo world!".to_string());

        assert!(!my_todo.is_complete());
        assert!(my_todo.mark_complete().is_complete());
        assert!(!my_todo.mark_incomplete().is_complete());
    }

    #[test]
    fn update_description_will_change_the_description() {
        let my_todo = Todo::from("hello, todo world!".to_string());
        assert_eq!(my_todo.description, "hello, todo world!");

        let my_todo_changed = my_todo.with_description("new hello, world!".to_string());
        assert_eq!(my_todo_changed.description, "new hello, world!");
    }

    #[test]
    fn mark_visible_function_marks_todo_visible() {
        let my_todo = Todo::from("hello, todo world!".to_string());

        assert!(my_todo.is_visible());
        assert!(my_todo.mark_visible().is_visible());
        assert!(my_todo.mark_visible().is_visible());
    }

    #[test]
    fn mark_invisible_function_marks_todo_invisible() {
        let my_todo = Todo::from("hello, todo world!".to_string());

        assert!(my_todo.is_visible());
        assert!(!my_todo.mark_invisible().is_visible());
        assert!(!my_todo.mark_invisible().is_visible());
    }
}
