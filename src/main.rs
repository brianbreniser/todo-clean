extern crate uuid;

pub mod entities;
pub mod interactors;
pub mod use_cases;

use entities::todo::Todo;
use interactors::in_memory_kvdb::InMemoryKVDB;
use use_cases::todo_store::TodoStore;

/*
 * Experiemntation, because dynamic dispatch in Rust is kinda rough
 */

trait InnerTrait {
    fn run(&self) {}
}

struct Inner {}
struct Other {}

impl InnerTrait for Inner {
    fn run(&self) {
        println!("it worked for inner")
    }
}

impl InnerTrait for Other {
    fn run(&self) {
        println!("it worked for Other")
    }
}

struct Concrete {
    inner: Box<InnerTrait>,
}

impl Concrete {
    fn new(x: Box<InnerTrait>) -> Concrete {
        Concrete { inner: x }
    }

    fn doit(&self) {
        self.inner.run()
    }
}

/*
 * main function, the only thing that actually belongs here full time
 */

fn main() {
    // dynamic disptach experiement
    let x = Inner {};
    let y = Other {};

    let c = Concrete::new(Box::new(x));
    let c2 = Concrete::new(Box::new(y));
    c.doit();
    c2.doit();

    // quick api use test

    let mut todo_list: Vec<Todo> = Vec::new();

    let db = InMemoryKVDB::new();
    let mut li = TodoStore::new(Box::new(db));

    let item = li.create_from("new todo item".to_string()).unwrap();
    todo_list.push(item);
    todo_list.push(li.create_from("totally works".to_string()).unwrap());

    println!("{:?}", todo_list);
}
