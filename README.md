# todo-clean

A clean architecture implementation of a TODO app.

## TL;DR

It's a basic todo app, but it is teaching me about good architecture and project structure growth over time.

## Wait, I need more info

Sure, so why make a TODO app? Usually to each you a new language, framework, api, or something similar.

But I'm using this app as a way to teach myself good architecture.

Okay, but why?

Well, I work in web development, where mostly people are just trying to put out fires and keep services up.

In theory, if I stick around anywhere for awhile, I'll learn good architecture, and how not to put myself in these situations in the first place.

For now, I need to learn these techniques on my own, to make it possible for me to write clean code sooner, rather than later.

## How to read this repo

It's probably going to be true that the git history will be as interesting as the code in it's latest state.

I'm going to try my best to write in a TDD fashion (Though the first commits will likely be POC).

I'm also going to develop a basic architecture based on Bob Martin's clean architcture (Easily google-able).

So in theory, it might be interesting to see how the repo evolves over time to match the spec better.

## This is dumb, it's just a todo app, you don't need architcture

You miss the point, I'm teaching myself good architecture through a basic app I don't have to think about. I know how a TODO app works, but I don't know how to write with clean architecture.

## When did this become a Q/A?

I dunno. I'll stop now

# Architecture

Reference: https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html

Overview:
```
_______________________
                       |
_________________      |
                 |     |
_______________  |     |
               | |     |
---------|     | |     |
Entities |     | |     | Entities are the 'highest level' of the app
_________|     | |     | Critical business logic is in the Entities
               | |     |
Use Case       | |     | Application specific business logic are in the use cases
_______________| |     |
                 |     |
Interactors      |     | Interactors are the interfaces that work with the outside world
_________________|     |
                       |
Implementation details | Implementation details are the 'lowest level'
_______________________| Implementaiton details are outside the project (psql, GUI framework)

```

The inner layers have more control than the outer layers, they controll their own API.

The outer layers gets closer to an implementation detail, and implementation details are exchangeable.

To move between layers requires an interface, defined by the inner layer, and implemented by the outer layer.

## So far, the architecture looks like this:

Entities:
    Todo

Use Cases:
    TodoStore
        Defines a Trait called: DataStoreTrait

Interactors:
    inMemoryKVDB (in memory key-value datastore)
        The inMemoryKVDB relies on a rust HashMap type (of HashMap<Uuid, Todo>)
        The inMemoryKVDB implements the DataStoreTrait Trait from the TodoStore module

So, we currently have:

```
______________________________________________________________
_____________________________________________                 |
___________                                  |                |
| Entities | Use Cases                       | Interactors    | The rest of the world is over here
|          |                                 |                |
|    Todo <-- TodoStore --> DataStoreTrait <<-- inMemoryKVDB --> HashMap
|__________|                                 |                |
_____________________________________________|                |
______________________________________________________________|
```

Notice:
    - Single lines: `-->` or `<--` indicate a hard dependency
    - Double arrowhead lines `-->>` or '<<--' indicate implementation of a Trait
    - All arrows always cross the boarder to the left (Implementation details rely on core-logic)
    - the inMemoryKVDB has an external-world dependency (HashMap in this case, but could be a real db)
        - the inMemoryKVDB is an interactor, and is the only thing that relies on external dependencies
    - the TodoStore defines it's own interface for talking to a DB (the DataStoreTrait)
    - Everything to the left of a boarder does NOT change based on what is the the right of a boarder

Short update:
    - I'm experiementing with using a controller-like infrastructure instead of a large TodoStore object
      Currently, the modified architecture looks like this

```
____________________________________________________________________
___________________________________________________                 |
___________                                        |                |
| Entities | Use Cases                             | Interactors    | The rest of the world is over here
|          | |- TodoStore ----|                    |                |
|    Todo <---- CreateNewTodo ---> DataStoreTrait <<-- inMemoryKVDB --> HashMap
|__________| |- RetrieveTodo -|                    |                |
___________________________________________________|                |
____________________________________________________________________|

```

Notice how I added a `CreateNewTodo` and `RetrieveTodo` controller/function

Those functions work the same basic way that the TodoStore works but only have 1 functionality

A calling routine can pick/choose what it uses based on its needs

Also notice in their implementation I've used a different abstraction method
    - Instead of a `Box<SomeTrait>` I've used a generic `<T: SomeTrait>`
    - This allows compile-time static dispatch (Rust calles it being monomorphic, or Trait Objects)

